app.config(function($routeProvider){
    $routeProvider.when('/',{
        templateUrl : 'pages/home.html',
        controller : 'HomeController'
    })
    .when('/about',{
        templateUrl : 'pages/about.html',
        controller : 'AboutController'  
    })
    .when('/contact',{
        templateUrl : 'pages/contact.html',
        controller : 'ContactController'  
    })
    .when('/param/:theparam',{
      templateUrl : 'pages/param.html',
      controller : 'ParamController'
    })
    .otherwise({
        redirectTo : '/'
    })
});