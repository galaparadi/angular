app.directive('myDirective',function(){
    return {
        // scope:true, //menjadikan scope child
        scope : {
            
        }, //ini isolated scope
        restrict:'EA',
        template:'<h2>Hello {{dunia}}</h2>',
        // replace: true //jika mau meletakan tanpa menyertai elemen directivenya
        link : function(scope,elem,attrs){
            elem.bind('click',function(){
              scope.dunia='Saya diklik';
              scope.$digest();
            });
        }
    };
});

app1.directive('agsTable',function(){ 
  return { 
    restrict:'E', 
    scope:{ 
        books:'=' 
    }, 
    templateUrl:'js/directives/AgsTable.html' 
  }; 
});